<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage FB Newsroom
 * @since FB Newsroom 1.0
 */
?>


               <div class="card post article-single" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="card-padding remove-padding-bottom">
					
                        <header class="">
                            <?php if ( is_single() ) : ?>
								<p class="date"><?php the_date(); ?></p>
							<?php endif; // is_single() ?>
							
							<?php if ( is_single() || is_page() ) : ?>
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php else : ?>
							<h1 class="entry-title">
								<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h1>
							<?php endif; // is_single() ?>
                        </header>

						<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
						<div class="entry-thumbnail">
							<div class="featured-image"><?php the_post_thumbnail(); ?></div>
						</div>
						<?php endif; ?>
						
                    </div><!--end:.card-padding-->

                    <div class=
                    "card-padding remove-padding-top remove-padding-bottom hide-overflow">
						<div class="post-content">
							<?php if ( is_search() ) : // Only display Excerpts for Search ?>
							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->
							<?php else : ?>
							<div class="entry-content">
								<?php
									/* translators: %s: Name of current post */
									the_content( sprintf(
										__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'fbnewsroom' ),
										the_title( '<span class="screen-reader-text">', '</span>', false )
									) );

									wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'fbnewsroom' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
								?>
							</div><!-- .entry-content -->
							<?php endif; ?>
                        </div><!--end:.post-content-->
                    </div><!--end:.card-padding-->

					<?php if ( is_single() ) : // Only display Excerpts for Single ?>
                    <div class="card-padding remove-padding-top">

                        <div class="entry-meta">
                            <div class="entry-category">
                                <span class="entry-meta-label">Category :</span><?php the_category(' '); ?>
								<?php edit_post_link( __( 'Edit', 'fbnewsroom' ), '<span class="edit-link">', '</span>' ); ?>
                            </div>
							
							<div class="entry-meta">
								<span class="entry-meta-label">Related News</span>
								<?php fbnewsroom_post_nav(); ?>
							</div>
							
							<?php comments_template(); ?>
							
                        </div><!--end:.entry-meta-->
                    </div><!--end:.card-padding-->
					<?php endif; ?>
					
                </div><!--end:.card.post.article-single-->

