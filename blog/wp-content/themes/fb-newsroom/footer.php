<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage FB Newsroom
 * @since FB Newsroom 1.0
 */
?>

    <div class="wrapper">
        <footer id="footer-primary">

            <p style="float:left;">
				<?php bloginfo( 'name' ); ?> &copy; <?php echo date('Y', time()); ?>

			</p>
        </footer><!--end:.footer-primary-->
    </div><!--end:.wrapper-->
    <?php wp_footer(); ?>
</body>
</html>