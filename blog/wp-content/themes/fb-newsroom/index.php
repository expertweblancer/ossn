<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FB Newsroom
 * @since FB Newsroom 1.0
 */

get_header(); ?>

    <section class="two-columns home-main">
        <div class="primary-content float">
            <div class="home-recent-news">
                <div class="card-padding clearfix">

                    <h2 class="heading">Recent News and
                    Announcements</h2><?php if ( have_posts() ) : ?><?php /* The loop */ ?><?php while ( have_posts() ) : the_post(); ?>

                    <div class="home-recent-news-item home-recent-news-item-1">
                        <p class="date"><?php echo get_the_date(); ?></p>

                        <h3><a href=
                        "<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                    </div><!--end:.home-recent-news-item-->
                    <?php endwhile; ?><?php fbnewsroom_paging_nav(); ?><?php else : ?><?php get_template_part( 'content', 'none' ); ?><?php endif; ?>

                    <div style=
                    "overflow: hidden; height: 0px; visibility: hidden;">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp;
                    </div>
                </div>
            </div><!--end:.home-recent-news-->
        </div><!--end:.primary-content-->

        <div class="secondary-content">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
        </div><!--end:.secondary-content-->
    </section><!-- Your Google Analytics Plugin is missing the tracking ID -->
    <!--end:.wrapper-->
	
<?php get_footer(); ?>
