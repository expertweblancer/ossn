<?php
/**
 * Open Source Social Network
 *
 * @package   Open Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

//this line is used to register initliize function to ossn system
ossn_register_callback('ossn', 'init', 'ossn_hello_world');

ossn_register_menu_item("topbar_dropdown", array(
                    "name" => "donate",
                    "text" => "Donate Please",
                    "href" => "Https://gofund.me/2nbkb4k ",
            ));
ossn_register_menu_item("topbar_dropdown", array(
                    "name" => "help	",
                    "text" => "Help",
                    "href" => "http://socialfeed.club/help",
            ));
ossn_register_menu_item("topbar_dropdown", array(
                    "name" => "Feedback	",
                    "text" => "Feedback",
                    "href" => "http://socialfeed.club/feedback/",
            ));          