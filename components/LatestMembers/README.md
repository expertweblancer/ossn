Latest Members
==============

V.1.0

This components will add two new widgets to the newsfeed sidebar on the right.
 - a list of the 15 latest community members
 - a list of your 15 latest friends

Besides that it makes a new 'All members' page available to your community
which can be reached by http://YOUR_SITE_URL/members/all

Important:
If you are still running the former "OssnMembers" component with Ossn 4.x,
we strongly recommend to remove it and use this one instead.
 
