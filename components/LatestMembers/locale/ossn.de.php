<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$de = array(
    'com:latestmembers:all:members' => 'Alle Mitglieder',
	'com:latestmembers:latest:members' => 'Neuste Mitglieder',
	'com:latestmembers:latest:friends' => 'Neuste Freunde',
);
ossn_register_languages('de', $de); 
