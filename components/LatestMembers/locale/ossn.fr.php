<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$fr = array(
    'com:latestmembers:all:members' => 'Tous les membres',
	'com:latestmembers:latest:members' => 'Nouveaux membres',
	'com:latestmembers:latest:friends' => 'Derniers amis',
);
ossn_register_languages('fr', $fr); 
