<?php
/**
* Open Source Social Network
*
* @package Open Source Social Network
* @author Open Social Website Core Team <info@informatikon.com>
* @copyright 2014 iNFORMATIKON TECHNOLOGIES
* @license General Public Licence http://www.opensource-socialnetwork.org/licence
* @link http://www.opensource-socialnetwork.org/licence
*/
define('__LATEST_MEMBERS__', ossn_route()->com . 'LatestMembers/');

function latest_members_init(){
	ossn_extend_view('css/ossn.default', 'css/latestmembers');
	if(ossn_isLoggedin()){
		ossn_register_page('members', 'members_pages');
		ossn_add_hook('newsfeed', 'sidebar:right', 'members_widget');
		ossn_add_hook('newsfeed', 'sidebar:right', 'friends_widget');
	}
}

/* use http://YOUR_SITE_URL/members/all to display a standalone page with members only */
function members_pages($pages) {
	$page = $pages[0];
	if (empty($page)) {
	redirect();
	exit;
	}
	switch ($page) {
		case 'all':
		$title = ossn_print('com:latestmembers:all:members');
		$page_params = array('widget_header' => $title);
		$page_content = array('content' => ossn_view('components/LatestMembers/pages/members', $page_params));
		$content = ossn_set_page_layout('contents', $page_content);
		echo ossn_view_page($title, $content);
		break;

		default;
		redirect();
		exit;
		break;
	}
}

function members_widget($hook, $type, $return){
	$widget_content = ossn_view('components/LatestMembers/widgets/members_widget');
	$widget = ossn_plugin_view('widget/view', array(
					'title' => ossn_print('com:latestmembers:latest:members'),
					'contents' => $widget_content
	));	
	$return[] = $widget;
	return $return;
}

function friends_widget($hook, $type, $return){
	$widget_content = ossn_view('components/LatestMembers/widgets/friends_widget');
	$widget = ossn_plugin_view('widget/view', array(
					'title' => ossn_print('com:latestmembers:latest:friends'),
					'contents' => $widget_content
	));	
	$return[] = $widget;
	return $return;
}

ossn_register_callback('ossn', 'init', 'latest_members_init');
