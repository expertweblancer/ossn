<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$user_class = new OssnUser;
$attr = array(
	'keyword' => false,
	'order_by' => 'guid DESC'
);
$users = $user_class->searchUsers($attr);
$count = $user_class->searchUsers(array('count' => true));
?>

<div class="ossn-layout-newsfeed">
<div class="col-md-12">
<div class="ossn-page-contents">
<div class="ossn-widget">
<div class="widget-heading"><?php echo $params['widget_header']; ?></div>
<div class="widget-contents">
<?php
foreach($users as $user) { ?>
	<a title="<?php echo $user->first_name . ' ' . $user->last_name; ?>"
	class="com-members-memberlist-item"
	href="<?php echo ossn_site_url() . 'u/' . $user->username; ?>">
	<img class="user-img" src="<?php echo $user->iconURL()->large;?>"></a>
<?php
}
echo ossn_view_pagination($count);
?>	
</div>		
</div>
</div>
</div>
</div>