<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$attr = array(
	 'limit' => 15
);
$friends = ossn_loggedin_user()->getFriends('', $attr);

if ($friends) {
	foreach($friends as $user) { ?>
		<a title="<?php echo $user->first_name . ' ' . $user->last_name; ?>"
		class="com-members-memberlist-item"
		href="<?php echo ossn_site_url() . 'u/' . $user->username; ?>">
		<img class="user-img" src="<?php echo $user->iconURL()->small;?>"></a>
		<?php
	}
}