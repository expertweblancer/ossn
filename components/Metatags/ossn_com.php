<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   http://www.webbehinds.com
 * @author    Sathish kumar S<info@opensource-socialnetwork.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES 
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */

define('__metatags_PATH__', ossn_route()->com . 'Metatags/');

function metatags_init() {
	//don't register again and again for each component
	//register at onece
	ossn_register_com_panel('metatags', 'settings');

	ossn_extend_view('ossn/site/head','pages/metatags');
	
	//add action for save settings
	if(ossn_isAdminLoggedin()){
		ossn_register_action('admin/metatags/settings', __metatags_PATH__ . 'actions/save.php');		
	}	
	
}

ossn_register_callback('ossn', 'init', 'metatags_init');
