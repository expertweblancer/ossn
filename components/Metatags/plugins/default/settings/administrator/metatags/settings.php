<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   http://www.webbehinds.com
 * @author    Sathish kumar S<info@opensource-socialnetwork.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES 
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */
 
echo ossn_view_form('administrator/settings', array(
    'action' => ossn_site_url() . 'action/admin/metatags/settings',
    'component' => 'metatags',
	'params' => $params,
    'class' => 'ossn-admin-form'	
), false);
