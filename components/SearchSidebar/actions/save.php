<?php
/**
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Claus Lampert <mai@charttec.de>
 * @copyright 2016 Claus Lampert Finanzinformationen
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 * Credits (most of components-sourcecode):
 * Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 */
$input = input('search');
$component = new OssnComponents;

$vars = array(
			 'free_search' => $input
		 );
if($component->setSettings('SearchSidebar', $vars)){
	ossn_trigger_message('Successfully saved html');
	redirect(REF);
} else {
	ossn_trigger_message('Can not save html', 'error');
	redirect(REF);
}