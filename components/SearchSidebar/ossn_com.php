<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Claus Lampert <mai@charttec.de>
 * @copyright 2016 Claus Lampert Finanzinformationen
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 * Credits (most of components-sourcecode):
 * Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 */

/**
 *
 * Define path of component
 */
define('__SEARCH_SIDEBAR__', ossn_route()->com . 'SearchSidebar/');

/**
 * Initialize the component and call function search_widget
 */
function search_sidebar(){
  ossn_add_hook('newsfeed', "sidebar:right", 'search_widget', -1);
  ossn_register_com_panel('searchsidebar', 'settings');
  if (ossn_isAdminLoggedin()) {
     ossn_register_action('search/sidebar/save', __SEARCH_SIDEBAR__ . 'actions/save.php');
     }
  }

/**
 * Show Search-Widget with search-slot in sidebar
 * code is stored in [plugins/default/] SearchSidebar/contents [.php]
 */
function search_widget($hook, $tye, $return){
  $return[] = ossn_plugin_view('searchsidebar/contents');
  return $return;
  }

/**
 * Convert save data into html code
 * @param string $text encoded html code.
 * return void|string
 */

/**
 * Admin Panel: dislay text
 */

  function search_sidebar_output($text = ''){
  if(!empty($text)){
	    return html_entity_decode($text, ENT_COMPAT, 'utf-8');
	    }
    }

ossn_register_callback('ossn', 'init', 'search_sidebar');
