<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Claus Lampert <mai@charttec.de>
 * @copyright 2016 Claus Lampert Finanzinformationen
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 * Credits (most of components-sourcecode):
 * Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 */

 /**
 This will be used later in Admin-Panel to store some HTML to be displayed above the search slot
 */

 ?>
 <div>
	 <label>Enter HTML-code to be displayed above the search-slot</label>
 	<textarea name="search"><?php $component = new OssnComponents;$settings  = $component->getSettings('searchsidebar');echo search_sidebar_output($settings->free_search);?></textarea>
 </div>
 <div>
 	<input type="submit" value="Save" class="btn btn-primary"/>
 </div>   