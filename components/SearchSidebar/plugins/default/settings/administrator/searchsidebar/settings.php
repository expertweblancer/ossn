<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Claus Lampert <mai@charttec.de>
 * @copyright 2016 Claus Lampert Finanzinformationen
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 * Credits (most of components-sourcecode):
 * Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 */

echo ossn_view_form('save', array(
    'action' => ossn_site_url() . 'action/search/sidebar/save',
    'component' => 'searchsidebar',
	'params' => $params,
), false);
