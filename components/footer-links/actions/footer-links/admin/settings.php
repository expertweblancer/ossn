<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
define('LINKS_TOTAL', 3);
$facebook_url = input('facebook_url');
$twitter_url = input('twitter_url');
$custom_url = input('custom_url');
$custom_name = input('custom_name');
$component = new OssnComponents;
$error_count = 0;

if(!empty($facebook_url) && !filter_var($facebook_url, FILTER_VALIDATE_URL)) {
	$error_facebook = ossn_print('com:footer-links:facebook:invalid');
	$error_count++;
} else {
	if(!$component->setSettings('footer-links', array('facebook_url' => $facebook_url))) {
		$error_count++;
	}
}

if(!empty($twitter_url) && !filter_var($twitter_url, FILTER_VALIDATE_URL)) {
	$error_twitter = ossn_print('com:footer-links:twitter:invalid');
	$error_count++;
} else {
	if(!$component->setSettings('footer-links', array('twitter_url' => $twitter_url))) {
		$error_count++;
	}
}

if((!empty($custom_url) && !filter_var($custom_url, FILTER_VALIDATE_URL)) || (!empty($custom_url) && empty($custom_name)) || (empty($custom_url) && !empty($custom_name))) {
	$error_custom = ossn_print('com:footer-links:custom:invalid');
	$error_count++;
} else {
	if(!$component->setSettings('footer-links', array('custom_url' => $custom_url)) || !$component->setSettings('footer-links', array('custom_name' => $custom_name))) {
		$error_count++;
	}
}

if($error_count) {
	$error_msg = $error_facebook . $error_twitter . $error_custom . ossn_print('com:footer-links:save:failed', array($error_count, LINKS_TOTAL));
	ossn_trigger_message($error_msg, 'error');
} else {
	ossn_trigger_message(ossn_print('com:footer-links:save:success'));
}

redirect(REF);