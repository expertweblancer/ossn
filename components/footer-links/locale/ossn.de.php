<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$de = array(
    'footer-links' => 'Footer Links',
	'com:footer-links:save:success' => 'Alle Links wurden erfolgreich gespeichert',
	'com:footer-links:save:failed' => ' %s von %s Links konnte(n) nicht gespeichert werden',
	'com:footer-links:facebook:url:label' => 'Facebook URL',
	'com:footer-links:facebook:invalid' => 'Facebook Link ungültig - ',
	'com:footer-links:twitter:url:label' => 'Twitter URL',
	'com:footer-links:twitter:invalid' => 'Twitter Link ungültig - ',
	'com:footer-links:custom:url:label' => 'Eigener Link URL',
	'com:footer-links:custom:name:label' => 'Eigener Link Name',
	'com:footer-links:custom:invalid' => 'Eigener Link ungültig - ',
	'com:footer-links:instruction' => 'Gib entweder eine gültige Url ein wie z.B. <i>http://some.site.com</i> - oder lasse das Feld leer.<br />Bei der eigenen URL muss zusätzlich ein sinnvoller Link Name mit angegeben werden.',
);
ossn_register_languages('de', $de); 
