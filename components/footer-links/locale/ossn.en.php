<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'footer-links' => 'Footer Links',
	'com:footer-links:save:success' => 'All links saved successfully',
	'com:footer-links:save:failed' => ' %s of %s links could not be saved',
	'com:footer-links:facebook:url:label' => 'Facebook URL',
	'com:footer-links:facebook:invalid' => 'Facebook link invalid - ',
	'com:footer-links:twitter:url:label' => 'Twitter URL',
	'com:footer-links:twitter:invalid' => 'Twitter link invalid - ',
	'com:footer-links:custom:url:label' => 'Custom link URL',
	'com:footer-links:custom:name:label' => 'Custom link name',
	'com:footer-links:custom:invalid' => 'Custom link invalid - ',
	'com:footer-links:instruction' => 'Please, enter a valid url like <i>http://some.site.com</i> - or leave blank.<br />With the custom url you have to choose a meaningful link name, too.',
);
ossn_register_languages('en', $en); 
