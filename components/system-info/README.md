System Info
===========

A support component to provide some useful informations about your server
and Ossn configuration.

visit http://YOUR_COMMUNITY_URL/ossninfo to view this page

V1.7
display size of database and data directory

V1.6
display MySQL environment

V1.5
dont' bomb on unavailable POSIX functions
display server OS

V1.4.1
fix for system dependencies in checksum computing

V1.4
md5 checksums of several important directories added

V1.3
data directory and directory permissions added

V1.2
Site Notification Email added to Ossn Environment

V1.1
additionally displays Ossn environment, modules and themes

V1.0
displays some basic PHP settings